import os
import sys

PATH = os.path.dirname(os.path.abspath(__file__))

sys.path.append(os.path.join(PATH, '..', 'src'))

import py_vscan

if __name__ == '__main__':

    dev = None
    if len (sys.argv) > 1:
        dev = py_vscan.PyVscan(sys.argv[1])
    else:
        dev = py_vscan.PyVscan()

    print dev

    dev.Open()

    print dev, dev.getApiVersion()

    # Start scan
    print dev.write(0x10, [0x00])

    print dev.flush()

    # read something
    print dev.read()

    dev.Close()

    print dev
