import os
import sys
import ctypes

PATH = os.path.dirname(os.path.abspath(__file__))

sys.path.append(os.path.join(PATH, '..', 'src'))

import py_vscan

from py_vscan.lib import *

if __name__ == '__main__':
    length = 2000
    error_string = (ctypes.c_char*length)()

    dev = VSCAN_FIRST_FOUND

    if len (sys.argv) > 1:
        dev = sys.argv[1]

    handle = VSCAN_Open(dev, VSCAN_MODE_NORMAL)

    if handle < 0:
        VSCAN_GetErrorString(handle, error_string, length)
        print handle, error_string.value

    print "Got handle to device"

    api = VSCAN_API_VERSION()
    res = VSCAN_Ioctl(handle, VSCAN_IOCTL_GET_API_VERSION, byref(api))
    if res != VSCAN_ERR_OK:
        VSCAN_GetErrorString(res, error_string, length)
        print res, error_string.value

    print api.Major, api.Minor, api.SubMinor
