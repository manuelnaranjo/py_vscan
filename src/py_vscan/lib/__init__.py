import platform
from ctypes import *
import os

PATH = os.path.dirname(os.path.abspath(__file__))

def tryLoad(name):
    try:
        return cdll.LoadLibrary(os.path.join(PATH, name))
    except Exception:
        return None

plats = platform.platform()

DLL = None

if 'linux' in plats.lower():
    for l in ['libvs_can_api.so', 'libvs_can_api_x86-64.so',
              'libvs_can_api_armel.so', 'libvs_can_api_armel_uclibc.so',
              'libvs_can_api_arm.so', 'libvs_can_api_arm_uclibc.so']:
        dll = tryLoad(l)
        if dll:
            DLL = dll
            break

if DLL is None:
    raise Exception("Platform not supported %s" % plats)

class VSCAN_HWPARAM(Structure):
    _fields_ = [ ('SerialNr', c_uint32),
                 ('HwVersion', c_uint8),
                 ('SwVersion', c_uint8),
                 ('HwType', c_uint8) ]

class VSCAN_MSG(Structure):
    _fields_ = [ ('Id', c_uint32),
                 ('Size', c_uint8),
                 ('Data', c_uint8*8),
                 ('Flags', c_uint8),
                 ('Timestamp', c_uint16)]

# Bit Timing Register Structure
class VSCAN_BTR(Structure):
    _fields_ = [ ('Btr0', c_uint8),
                 ('Btr1', c_uint8) ]

# Acceptance Code and Mask Structure
class VSCAN_CODE_MASK(Structure):
    _fields_ = [ ('Code', c_uint32),
                 ('Mask', c_uint32) ]

# API Version Structure
class VSCAN_API_VERSION(Structure):
    _fields_= [ ('Major', c_uint8),
                ('Minor', c_uint8),
                ('SubMinor', c_uint8)]

# external structures
class sem_t(Structure):
    pass

VSCAN_FIRST_FOUND = POINTER(c_char)()

def __ioctl_value_pointer__(val):
    return POINTER(c_ulong)(c_ulong(val))

# Debug Mode
VSCAN_DEBUG_MODE_CONSOLE = __ioctl_value_pointer__(1)
VSCAN_DEBUG_MODE_FILE = __ioctl_value_pointer__(1)

# Debug Level
VSCAN_DEBUG_NONE = __ioctl_value_pointer__(0)
VSCAN_DEBUG_LOW = __ioctl_value_pointer__(-1)
VSCAN_DEBUG_MID = __ioctl_value_pointer__(-51)
VSCAN_DEBUG_HIGH = __ioctl_value_pointer__(-101)

VSCAN_STATUS = c_int
VSCAN_HANDLE = c_int

# Status / Errors
VSCAN_ERR_OK = VSCAN_STATUS(0)
VSCAN_ERR_ERR = VSCAN_STATUS(-1)
VSCAN_ERR_NO_DEVICE_FOUND = VSCAN_STATUS(-2)
VSCAN_ERR_SUBAPI = VSCAN_STATUS(-3)
VSCAN_ERR_NOT_ENOUGH_MEMORY = VSCAN_STATUS(-4)
VSCAN_ERR_NO_ELEMENT_FOUND = VSCAN_STATUS(-5)
VSCAN_ERR_INVALID_HANDLE = VSCAN_STATUS(-6)
VSCAN_ERR_IOCTL = VSCAN_STATUS(-7)
VSCAN_ERR_MUTEX = VSCAN_STATUS(-8)
VSCAN_ERR_CMD = VSCAN_STATUS(-9)
VSCAN_ERR_LISTEN_ONLY = VSCAN_STATUS(-10)
VSCAN_ERR_NOT_SUPPORTED = VSCAN_STATUS(-11)
VSCAN_ERR_GOTO_ERROR = VSCAN_STATUS(-101)

# Mode
VSCAN_MODE_NORMAL = c_ulong(0)
VSCAN_MODE_LISTEN_ONLY = c_ulong(1)
VSCAN_MODE_SELF_RECEPTION = c_ulong(2)

# Speed
VSCAN_SPEED_1M = __ioctl_value_pointer__(8)
VSCAN_SPEED_800K = __ioctl_value_pointer__(7)
VSCAN_SPEED_500K = __ioctl_value_pointer__(6)
VSCAN_SPEED_250K = __ioctl_value_pointer__(5)
VSCAN_SPEED_125K =__ioctl_value_pointer__(4)
VSCAN_SPEED_100K = __ioctl_value_pointer__(3)
VSCAN_SPEED_50K = __ioctl_value_pointer__(2)
VSCAN_SPEED_20K = __ioctl_value_pointer__(1)
# generally not possible with the TJA1050
VSCAN_SPEED_10K = __ioctl_value_pointer__(0)

# Device Types
VSCAN_HWTYPE_UNKNOWN = c_uint8(0)
VSCAN_HWTYPE_SERIAL = c_uint8(1)
VSCAN_HWTYPE_USB = c_uint8(2)
VSCAN_HWTYPE_NET = c_uint8(3)
VSCAN_HWTYPE_BUS = c_uint8(4)

VSCAN_IOCTL_OFF = __ioctl_value_pointer__(0)
VSCAN_IOCTL_ON = __ioctl_value_pointer__(1)

# Timestamp
VSCAN_TIMESTAMP_OFF = VSCAN_IOCTL_OFF
VSCAN_TIMESTAMP_ON = VSCAN_IOCTL_ON

# Filter Mode
VSCAN_FILTER_MODE_SINGLE = __ioctl_value_pointer__(1)
VSCAN_FILTER_MODE_DUAL = __ioctl_value_pointer__(2)

# Ioctls
VSCAN_IOCTL_SET_DEBUG = c_ulong(1)
VSCAN_IOCTL_GET_HWPARAM = c_ulong(2)
VSCAN_IOCTL_SET_SPEED = c_ulong(3)
VSCAN_IOCTL_SET_BTR = c_ulong(4)
VSCAN_IOCTL_GET_FLAGS = c_ulong(5)
VSCAN_IOCTL_SET_ACC_CODE_MASK = c_ulong(6)
VSCAN_IOCTL_SET_TIMESTAMP = c_ulong(7)
VSCAN_IOCTL_SET_DEBUG_MODE = c_ulong(8)
VSCAN_IOCTL_SET_BLOCKING_READ = c_ulong(9)
VSCAN_IOCTL_SET_FILTER_MODE = c_ulong(10)
VSCAN_IOCTL_GET_API_VERSION = c_ulong(11)

# Bits for VSCAN_IOCTL_GET_FLAGS
VSCAN_IOCTL_FLAG_RX_FIFO_FULL = VSCAN_STATUS(1 << 0)
VSCAN_IOCTL_FLAG_TX_FIFO_FULL = VSCAN_STATUS(1 << 1)
VSCAN_IOCTL_FLAG_ERR_WARNING = VSCAN_STATUS(1 << 2)
VSCAN_IOCTL_FLAG_DATA_OVERRUN = VSCAN_STATUS(1 << 3)
VSCAN_IOCTL_FLAG_UNUSED = VSCAN_STATUS(1 << 4)
VSCAN_IOCTL_FLAG_ERR_PASSIVE = VSCAN_STATUS(1 << 5)
VSCAN_IOCTL_FLAG_ARBIT_LOST = VSCAN_STATUS(1 << 6)
VSCAN_IOCTL_FLAG_BUS_ERROR = VSCAN_STATUS(1 << 7)
VSCAN_IOCTL_FLAG_API_RX_FIFO_FULL = VSCAN_STATUS(1 << 16)

# Masks for VSCAN_IOCTL_SET_ACC_CODE_MASK
VSCAN_IOCTL_ACC_CODE_ALL = c_uint32(0x00000000)
VSCAN_IOCTL_ACC_MASK_ALL = c_uint32(0xFFFFFFFF)

# Flags
VSCAN_FLAGS_STANDARD = c_uint8(1 << 0)
VSCAN_FLAGS_EXTENDED = c_uint8(1 << 1)
VSCAN_FLAGS_REMOTE = c_uint8(1 << 2)
VSCAN_FLAGS_TIMESTAMP = c_uint8(1 << 3)

# If the function succeeds, the return value is greater zero (handle)
# If the function fails, the return value is one of VSCAN_STATUS
VSCAN_Open = getattr(DLL, 'VSCAN_Open')
VSCAN_Open.restype = VSCAN_HANDLE
VSCAN_Open.argtypes = [ POINTER(c_char), c_ulong ]

# The return value is one of VSCAN_STATUS
VSCAN_Close = getattr(DLL, 'VSCAN_Close')
VSCAN_Close.restype = VSCAN_STATUS
VSCAN_Close.argtypes = [ VSCAN_HANDLE ]

# The return value is one of VSCAN_STATUS
VSCAN_Ioctl = getattr(DLL, 'VSCAN_Ioctl')
VSCAN_Ioctl.restype = VSCAN_STATUS
VSCAN_Ioctl.argtypes = [ VSCAN_HANDLE, c_ulong, c_voidp ]

# The return value is one of VSCAN_STATUS
VSCAN_Flush = getattr(DLL, 'VSCAN_Flush')
VSCAN_Flush.restype = VSCAN_STATUS
VSCAN_Flush.argtypes = [ VSCAN_HANDLE ]

# The return value is one of VSCAN_STATUS
VSCAN_Write = getattr(DLL, 'VSCAN_Write')
VSCAN_Write.restype = VSCAN_STATUS
VSCAN_Write.argtypes = [ VSCAN_HANDLE, POINTER(VSCAN_MSG), c_ulong,
                         POINTER(c_ulong) ]

# The return value is one of VSCAN_STATUS
VSCAN_Read = getattr(DLL, 'VSCAN_Read')
VSCAN_Read.restype = VSCAN_STATUS
VSCAN_Read.argtypes = [ VSCAN_HANDLE, POINTER(VSCAN_MSG), c_ulong,
                        POINTER(c_ulong)]

#The return value is one of VSCAN_STATUS
VSCAN_SetRcvEvent = getattr(DLL, 'VSCAN_SetRcvEvent')
VSCAN_SetRcvEvent.restype = VSCAN_STATUS
if 'windows' in plats:
    VSCAN_SetRcvEvent.argtypes = [ VSCAN_HANDLE, wintypes.HANDLE ]
else:
    VSCAN_SetRcvEvent.argtypes = [ VSCAN_HANDLE, POINTER(sem_t) ]

# No return value for this function
VSCAN_GetErrorString = getattr(DLL, 'VSCAN_GetErrorString')
VSCAN_GetErrorString.restype = None
VSCAN_GetErrorString.argtypes = [ VSCAN_STATUS, POINTER(c_char), c_ulong ]
