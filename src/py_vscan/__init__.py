import lib
from ctypes import *

class PyVscanException(Exception):
    def __init__(self, code):
        buff = (c_char*1024)()
        lib.VSCAN_GetErrorString(code, buff, 1024)
        self.code = code
        self.text = buff.value
        Exception.__init__(self, buff.value)

    def __str__(self):
        return "VSCAN error<%i>: %s" % (self.code, self.text)

class PyVscanNoOpenException(Exception):
    def __init__(self, text):
        Exception.__init__(self, "Need to call open first")

class PyWriteTooShortException(Exception):
    def __init__(self, expected, sent):
        Exception.__init__(self, "Expected to send %i, sent %i" % (expected, sent))

class PyVscan:
    portname = None
    handle = None
    api_version = None

    def __init__(self, portname=lib.VSCAN_FIRST_FOUND,
                 mode=lib.VSCAN_MODE_NORMAL):
        self.portname = portname
        self.mode = mode

    def Open(self):
        self.handle = lib.VSCAN_Open(self.portname, self.mode)
        if self.handle < 0:
            raise PyVscanException(self.handle)

    def Close(self):
        if self.handle is None:
            raise PyVscanNoOpenException()

        res = lib.VSCAN_Close(self.handle)
        if res != lib.VSCAN_ERR_OK.value:
            raise PyVscanException(res)

        self.handle = None

    def __ioctl__(self, ioctl, value):
        res = lib.VSCAN_Ioctl(self.handle, ioctl, byref(value))
        if res != lib.VSCAN_ERR_OK.value:
            raise PyVscanException(res)
        return res

    def getApiVersion(self):
        if self.handle is None:
            raise PyVscanNoOpenException()

        if self.api_version:
            return self.api_version

        api = lib.VSCAN_API_VERSION()
        self.__ioctl__(lib.VSCAN_IOCTL_GET_API_VERSION, api)

        self.api_version = [ api.Major, api.Minor, api.SubMinor ]

        return self.api_version

    def read(self, length=None):
        if self.handle is None:
            raise PyVscanNoOpenException()

        if length is None:
            length = 0

        received = c_ulong(0)
        count = (length/8)+1

        msgs = (lib.VSCAN_MSG*count)()

        res = lib.VSCAN_Read(self.handle, cast(msgs, POINTER(lib.VSCAN_MSG)),
                             count, byref(received))

        if res != lib.VSCAN_ERR_OK.value:
            raise PyVscanException(res)

        out = []

        for i in range(received.value):
            for j in range(8):
                out.append(msgs[i].Data[j].value)

        return out

    def write(self, Id, data, flags = lib.VSCAN_FLAGS_STANDARD):
        if self.handle is None:
            raise PyVscanNoOpenException()


        ldata = len(data)
        packs = (ldata/8)+1
        msgs = (lib.VSCAN_MSG*packs)()
        length = 0
        l = 0
        i = 0

        for msg in msgs:
            msg.Id = c_uint32(Id)
            if ldata - length > 8:
                l = 8
            else:
                l = (ldata - length)
            msg.Size = c_uint8(l)
            for i in range(0, l):
                msg.Data[i] = c_uint8(data[length+i])
            msg.Flags = flags

            Id = Id+1
            length = length + l

        sent = c_ulong(0)
        res = lib.VSCAN_Write(self.handle, cast(msgs, POINTER(lib.VSCAN_MSG)),
                              packs, byref(sent))
        if res != lib.VSCAN_ERR_OK.value:
            raise PyVscanException(res)

        if sent.value != ldata:
            raise

        return sent.value

    def flush(self):
        if self.handle is None:
            raise PyVscanNoOpenException()

        res = lib.VSCAN_Flush(self.handle)

        if res != lib.VSCAN_ERR_OK.value:
            raise PyVscanException(res)

        return True

    def __str__(self):
        o = "opened" if self.handle else "closed"
        p = self.portname if self.portname!=lib.VSCAN_FIRST_FOUND else \
            "FIRST_FOUND"
        return "PyVscan<%s,%s>" % (p, o)
